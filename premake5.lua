function get_cli_output(command)
    local handle = io.popen(command)
    local result = handle:read("*a")
    handle:close()
    return result
end


workspace "missile_launcher"
    configurations { "Debug", "Release" }
    project "missile_launcher"
        kind "WindowedApp"
        language "C"
        --targetdir "bin/%{cfg.buildcfg}"
        targetdir "."
        files { "src/**.h", "src/**.c" }
        
        buildoptions { get_cli_output("pkg-config --cflags gtk+-3.0") }
        linkoptions { get_cli_output("pkg-config --libs gtk+-3.0"), get_cli_output("pkg-config gmodule-2.0") }

        filter "configurations:Debug"
            defines { "DEBUG" }
            symbols "On"

        filter "configurations:Release"
            defines { "NDEBUG" }
            optimize "On"

#include <gtk/gtk.h>
#include <stdbool.h>

int main(int argc, char** argv) {
  gtk_init(&argc, &argv);
  GtkBuilder* builder =
      gtk_builder_new_from_file("src/missile_launcher_gui.glade");
  GtkWidget* window =
      GTK_WIDGET(gtk_builder_get_object(builder, "main_window"));
  g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

  gtk_builder_connect_signals(builder, NULL);
  gtk_widget_show(window);
  gtk_main();
  return 0;
}